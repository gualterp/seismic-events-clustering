"""
To obtain the k-distance graph, run calc_epsilon()
To execute each algorithm, run: 
    run_kmeans()
    run_dbscan()
    run_gaussian_mixture()
"""

import pandas as pd
import numpy as np, matplotlib.pyplot as plt
from sklearn.metrics import adjusted_rand_score, silhouette_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cluster import KMeans, DBSCAN
from sklearn.mixture import GaussianMixture
from plot_clusters import plot_classes

#------------------------------------------------------------------------#
# Data Processing

def transform_coordinates(lat, lon):
    """
    Transforms latitude and longitude in
    cartesian coordinates (x,y,z)
    """
    radius = 6371
    x = radius * np.cos(lat * np.pi/180) * np.cos(lon * np.pi/80)
    y = radius * np.cos(lat * np.pi/180) * np.sin(lon * np.pi/80)
    z = radius * np.sin(lat * np.pi/180)
    return x, y, z


def get_data():
    """
    Reads data from csv file to a dataframe,
    selects the needed fields and transforms latitude and longitude
    """
    dframe = pd.read_csv("sa.csv")
    lat = dframe.latitude
    lon = dframe.longitude
    faults = dframe.fault
    
    x, y, z = transform_coordinates(lat,lon)    
    features = np.stack([x,y,z], axis=1)  
    
    return features, faults, lat, lon 

    
#------------------------------------------------------------------------#
# K-Means
    
def kmeans(k, features):
    """
    Given a certain k value and features, returns the predicted labels
    by adjusting a model with K-Means algorithm 
    """
    km = KMeans(n_clusters=k, random_state=0)
    labels = km.fit_predict(features)    
    return labels    

    
def run_kmeans():
    """
    Parametrizes the K-Means algorithm with differente values,
    then calculates internal and external indexes to find out the best one.
    Finally, the model is adjusted with the found best value,
    and the clustering plotted.
    """
    features, faults, lat, lon = get_data()
    metrics = []    

    for k in range(10, 40):
        labels = kmeans(k, features)
        print("Current K: ", k)
        prec, recall, rand, f1, ari, s_score, _, _ = comp_scores(labels, faults, features)
        metrics.append((k, prec, recall, rand, f1, ari, s_score))  
        
    plot_metrics(metrics, "K")  
    
    best_k = 17 
    labels_best_k = kmeans(best_k, features)
    precision, recall, rand, f1, adj_rand_score, s_score, tp, tn = comp_scores(labels_best_k, faults, features)
       
    """   
    # For report purposes
    print("********************************************")
    print("True Positives: ", tp)
    print("True Negatives: ", tn)
    """    
    plot_classes(labels_best_k, lon, lat)
    
#------------------------------------------------------------------------#
# Gaussian Mixture
    
def gaussian_mixture(gc, features):
    """
    Given a certain number of gaussian components and features,
    returns the predicted labels by adjusting a model with 
    Gaussian Mixture Model algorithm / Expectation-Maximization
    """
    gm = GaussianMixture(n_components=gc, random_state=0)
    gm.fit(features)
    labels = gm.predict(features)
    return labels


def run_gaussian_mixture():
    """
    Parametrizes the GMM algorithm with differente gaussian component values,
    then calculates internal and external indexes to find out the best one.
    Finally, the model is adjusted with the found best value,
    and the clustering plotted.
    """
    features, faults, lat, lon = get_data()
    metrics = []
   
    for gc in range(10, 50):
        labels = gaussian_mixture(gc, features)
        print("Current GC: ", gc)
        prec, recall, rand, f1, ari, s_score, _ , _ =  comp_scores(labels, faults, features)
        metrics.append((gc, prec, recall, rand, f1, ari, s_score))  
        
    plot_metrics(metrics, "Gaussian Components")  
    
    best_gc = 21 
    labels_best_gc = gaussian_mixture(best_gc, features)
    plot_classes(labels_best_gc, lon, lat)

#------------------------------------------------------------------------#
# DBSCAN
       
def plot_dists(distances):    
    """
    Plots the k-distance graph
    """
    fig = plt.figure(figsize=(6,6), frameon=False)    
    plt.plot(distances, '-', linewidth=1)
    plt.xlabel('Cluster Elements')
    plt.ylabel('Distance')
    fig.savefig('elbow.png', dpi=300, bbox_inches='tight')
    plt.show()
    plt.close()
    
    
def calc_epsilon():
    """
    Creates the k-distance graph, allowing the
    selection of epsilon parameter for DBSCAN
    """
    features, _, _, _ = get_data() 
    neighbors = 4 # As suggested in the paper
    
    # Labels array filled with 1's since a good adjustment is not the goal
    fake_labels = np.ones(features.shape[0]) 
    reg = KNeighborsClassifier(neighbors)    
    reg.fit(features, fake_labels)
    nearest_neighbors, _ = reg.kneighbors()  
      
    max_dist = np.max(nearest_neighbors, axis=1)    
    sorted_max_dist = np.sort(max_dist)
    sorted_max_dist = sorted_max_dist[::-1]
 
    plot_dists(sorted_max_dist)
    
    
def dbscan(epsilon, features):
    """
    Given a certain epsilon value and features, returns the predicted labels
    by adjusting a model with DBSCAN algorithm 
    """
    dbs = DBSCAN(eps=epsilon, min_samples=4)
    labels = dbs.fit_predict(features)
    return labels


def run_dbscan():
    """
    Parametrizes the DBSCAN algorithm with different epsilon values,
    then calculates internal and external indexes to find out the best one.
    Finally, the model is adjusted with the found best value,
    and the clustering plotted.
    """
    features, faults, lat, lon = get_data() 
    metrics = []
    epsilons = range(20, 300, 10)
    
    for eps in epsilons:
        print("Current Epsilon: ", eps)
        labels = dbscan(eps, features)
        prec, recall, rand, f1, ari, s_score, _, _ = comp_scores(labels, faults, features)
        metrics.append((eps, prec, recall, rand, f1, ari, s_score))    
    plot_metrics(metrics, "Epsilon")  
    
    best_epsilon = 260 
    labels_best_epsilon = dbscan(best_epsilon, features)
    plot_classes(labels_best_epsilon, lon, lat)
    
    
#------------------------------------------------------------------------#
# Plot
    
def plot_metrics(metrics, parameter):
    """
    Plots the indexes's values for all parameter values tested
    """
    metrics = np.array(metrics)
    plt.figure(figsize=(12, 8), frameon=False)
    plt.plot(metrics[:, 0], metrics[:, 1:], linewidth=2)
    plt.legend(labels=['Precision', 'Recall', 'Rand', 'F1', 
                       'Adjusted', 'Silhouette'])
    plt.xlabel(parameter)
    plt.ylabel('Valores dos Índices')    
    plt.show()
    plt.close()
    
    
#------------------------------------------------------------------------#
# Clustering Scores

def comp_scores(labels, faults, features):
    """
    Computes all internal and external indexes
    """
    sf = sc = tp= tn = 0.0
    
    for ix in range(len(labels) - 1):
        same_fault = faults[ix] == faults[ix+1:]
        same_cluster = labels[ix] == labels[ix+1:]
        sf += np.sum(same_fault)
        sc += np.sum(same_cluster)
        tp += np.sum(np.logical_and(same_fault, same_cluster))
        tn += np.sum(np.logical_and(np.logical_not(same_fault),
                                    np.logical_not(same_cluster)))
    
    total = len(labels) * len(labels - 1)/2
    precision = tp/sc
    recall = tp/sf
    rand = (tp+tn)/total
    f1 = precision*recall*2/(precision+recall)
    adj_rand_score = adjusted_rand_score(labels, faults)
    s_score = silhouette_score(features, labels)
        
    return precision, recall, rand, f1, adj_rand_score, s_score, tp, tn
       